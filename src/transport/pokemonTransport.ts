import IPokemon from "@/model/transport/template/pokemon";
import IPokemons from "@/model/transport/template/pokemons";
import IPokemonTransport from "@/model/transport/transport";
import axios from "axios";

class PokemonTransport implements IPokemonTransport{
    private static instance:IPokemonTransport;
    private apiUrl:string;
    
    private constructor(url:string){
        this.apiUrl = url;
    }

    public static getInstance(url:string): IPokemonTransport {
        if (!PokemonTransport.instance) {
            PokemonTransport.instance = new PokemonTransport(url);
        }
        return PokemonTransport.instance;
    }

    public async fetchPokemons(url:string, offset:number = 0, limit:number = 20):Promise<IPokemons> {
        try {
            const response = await axios.get(url ?? this.apiUrl, {
                params: {
                    offset: offset,
                    limit: limit,
                }
            });
            const data: IPokemons = response.data;
            return data;
        } catch (error) {
            if (axios.isAxiosError(error)) {
                throw new Error(`FetchPokemons: Error fetching data from API: ${error.response?.status} ${error.response?.statusText}`);
            } else {
                throw new Error(`FetchPokemons: An unexpected error occurred: ${error.message}`);
            }
        }
    }

    public async fetchPokemon(url:string):Promise<IPokemon> {
        try {
            const response = await axios.get(url);
            const data: IPokemon = response.data;
            return data;
        } catch (error) {
            if (axios.isAxiosError(error)) {
                throw new Error(`FetchPokemon: Error fetching data from API: ${error.response?.status} ${error.response?.statusText}`);
            } else {
                throw new Error(`FetchPokemon: An unexpected error occurred: ${error.message}`);
            }
        }
    }

    public async fetchPokemonById(id:string):Promise<IPokemon> {
        try {
            const response = await axios.get(this.apiUrl + "/" + id);
            const data: IPokemon = response.data;
            return data;
        } catch (error) {
            if (axios.isAxiosError(error)) {
                throw new Error(`FetchPokemon: Error fetching data from API: ${error.response?.status} ${error.response?.statusText}`);
            } else {
                throw new Error(`FetchPokemon: An unexpected error occurred: ${error.message}`);
            }
        }
    }
}

export default PokemonTransport.getInstance('https://pokeapi.co/api/v2/pokemon/')