

interface IPokemonFormated{
    id:number;
    name:string;
    imgUrl:string;
    types:Array<{
            type:string;
    }>
    abilities:Array<{
            ability:string
    }>
}

export default IPokemonFormated;