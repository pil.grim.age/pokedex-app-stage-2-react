import IPokemon from "../transport/template/pokemon";
import IPokemons from "../transport/template/pokemons";

interface IPokemonService{
    getPokemons(url?:string, limit?:number, page?:number):Promise<IPokemons>;
    getPokemon(url:string):Promise<IPokemon>;
    getPokemonById(id:string):Promise<IPokemon>;
    getFormatedPokemons(pokemons:IPokemons):Promise<IPokemon[]>
}

export default IPokemonService;