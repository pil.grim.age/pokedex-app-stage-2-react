import IPokemon from "../transport/template/pokemon";

interface IPaginationService{
    next():Promise<IPokemon[]>;
    prev():Promise<IPokemon[]>;
    getPage():number;
    getTotalPage():number;
}

export default IPaginationService;