import IPokemonFormated from "@/model/store/pokemonFormated";
import IPokemon from "@/model/transport/template/pokemon";

function formatName(name: string):string {
    return name
            .split('-')
            .map(word => word.charAt(0).toUpperCase() + word.slice(1).toLowerCase())
            .join(' ');
}

function formatPokemon(pokemon:IPokemon):IPokemonFormated{
    return {
        id: pokemon.id,
        name: pokemon.name,
        imgUrl: pokemon.sprites.front_default,
        types: pokemon.types.map(item=>({type:item.type.name})),
        abilities: pokemon.abilities.map(item=>({ability:item.ability.name})),
    }
}

export {formatName, formatPokemon}