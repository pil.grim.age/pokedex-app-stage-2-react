import IPokemon from "@/model/transport/template/pokemon";
import IPokemons from "@/model/transport/template/pokemons";
import IPokemonService from "@/model/service/pokemonService";
import IPokemonTransport from "@/model/transport/transport";
import pokemonTransport from "@/transport/pokemonTransport";

class PokemonService implements IPokemonService{
    
    private static instance:IPokemonService;
    private transport:IPokemonTransport;

    private constructor(transport:IPokemonTransport){
        this.transport = transport;
    }

    public static getInstance(transport:IPokemonTransport): IPokemonService {
        if (!PokemonService.instance) {
            PokemonService.instance = new PokemonService(transport);
        }
        return PokemonService.instance;
    }

    public async getPokemons(url?:string, limit?:number, page?:number):Promise<IPokemons>{
        try{
            const pokemons:IPokemons = await this.transport.fetchPokemons(url, (page-1)*limit, limit);
            return pokemons;
        }
        catch(error){
            throw new Error(`error at pokemonService.getPokemons\n${error.message}\nError fetching data from API: ${error.response?.status} ${error.response?.statusText}`);
        }
    }

    public async getFormatedPokemons(pokemons:IPokemons):Promise<IPokemon[]>{
        
        let promices:Promise<IPokemon>[] = pokemons.results.map(async pokemon=>(await this.getPokemon(pokemon.url)));
        let formatedPokemons:IPokemon[] = await Promise.all(promices);
        return formatedPokemons;
    }

    public async getPokemon(url:string):Promise<IPokemon>{
        try{
            return await this.transport.fetchPokemon(url)
        }
        catch(error){
            throw new Error(`error at pokemonService.getPokemon\n${error.message}\nError fetching data from API: ${error.response?.status} ${error.response?.statusText}`);
        }
    }

    public async getPokemonById(id:string):Promise<IPokemon>{
        try{
            return await this.transport.fetchPokemonById(id);
        }
        catch(error){
            throw new Error(`error at pokemonService.getPokemon\n${error.message}\nError fetching data from API: ${error.response?.status} ${error.response?.statusText}`);
        }
    }
}

export default PokemonService.getInstance(pokemonTransport);