import { Route, createBrowserRouter, createRoutesFromElements } from "react-router-dom";
import Home from "../pages/Home";
import Layout from "../components/layout/Layout";
import AllPokemons from "../pages/AllPokemons";
import MyPokemons from "../pages/MyPokemons";
import PokemonPage from "../pages/PokemonPage";
import Error from "../pages/Error";


const router = createBrowserRouter(createRoutesFromElements(
    <Route path="/" element={<Layout />}>
        <Route index element={<Home />}/>
        <Route path="my-pokemon" element={<MyPokemons />}/>
        <Route path="pokemon" element={<AllPokemons />}/>
        <Route path="pokemon/:id" element={<PokemonPage />}/>
        <Route path="*" element={<Error />}></Route>
    </Route>
));

export default router;