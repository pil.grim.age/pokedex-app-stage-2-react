import DeckSpace from "../components/UI/DeckSpace";
import Heading from "../components/UI/Heading";
import styles from "./../components/pokemons/styles/pokemonDeck.module.scss"

export default function Home(){
    return(
        <div className={styles.pokemonDeck}>
            <DeckSpace>
                <Heading>
                    Hello pokemon hunter
                </Heading>
            </DeckSpace>
        </div>
    );
}