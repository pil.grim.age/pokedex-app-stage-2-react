import PokemonAbout from "../components/pokemons/PokemonAbout";
import { useParams } from "react-router-dom";
import BackButt from "../components/UI/BackButt";

export default function PokemonPage(){
    const {id} = useParams();

    return(
        <>
            <BackButt/>
            <PokemonAbout id={id}/>
        </>
    );
}