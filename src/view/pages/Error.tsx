import BackButt from "../components/UI/BackButt";
import DeckSpace from "../components/UI/DeckSpace";
import Heading from "../components/UI/Heading";
import styles from "./../components/pokemons/styles/pokemonDeck.module.scss"

export default function Error(){
    return(
        <>
            <BackButt/>
            <div className={styles.pokemonDeck}>
                <DeckSpace>
                    <Heading>
                        Error 404: wrong address
                    </Heading>
                </DeckSpace>
            </div>
        </>
        
    );
}