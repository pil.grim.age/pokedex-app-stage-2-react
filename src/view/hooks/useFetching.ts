import { useState } from "react"
import { Type } from "typescript";

export default function useFetching(callback:(...args: any[]) => Promise<void>):[(...args: any[]) => Promise<void>, boolean, string] {
    const [isLoading, setIsLoading] = useState(false);
    const [error, setError] = useState('');

    const fetching = async (...args: any[]) => {
        try {
            setIsLoading(true);
            await callback(...args);
        } catch (e) {
            setError(e.message || 'An error occurred');
        } finally {
            setIsLoading(false);
        }
    }
    return [fetching, isLoading, error];
}