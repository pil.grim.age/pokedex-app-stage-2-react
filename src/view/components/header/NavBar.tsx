import { Link, NavLink, Route } from "react-router-dom";
import styles from "./styles/navBar.module.scss";

const setActive = (({isActive}:{isActive:boolean;}) => isActive ? styles.active : "");

export default function NavBar(){
    return(
        <nav className={styles.navBar}>
            <NavLink to="/pokemon" className={setActive}>POKEMONS</NavLink>
            <NavLink to="/my-pokemon" className={setActive}>MY POKEMONS</NavLink>
        </nav>
    );
    
}