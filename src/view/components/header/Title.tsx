import { Link, NavLink } from "react-router-dom";
import styles from "./styles/title.module.scss"


export default function Title(){
    return(
        <div className={styles.wrapper}>
            <h1 className={styles.title}>
                <NavLink to="/">Pokedex</NavLink>
                <span className={styles.sub}>
                    catch the pokemons
                </span>
            </h1>
        </div>
    );
}