import ToolButt from "../UI/ToolButt";
import styles from "./styles/tools.module.scss";

export default function Tools(){
    return(
        <div className={styles.tools}>
            <ToolButt name="lang" icon="https://static.tildacdn.com/tild3039-6434-4961-b363-343162373637/photo.png" event={()=>{}} />
            <ToolButt name="theme" icon="https://upload.wikimedia.org/wikipedia/commons/thumb/b/b5/NYCS-SSI-nightsonly.svg/2048px-NYCS-SSI-nightsonly.svg.png" event={()=>{}}/>
        </div>
    );
}