import NavBar from "./NavBar";
import Title from "./Title";
import Tools from "./Tools";
import styles from "./styles/header.module.scss";

export default function Header(){
    return(
        <header className={styles.header}>
            <Title/>
            <div className={styles.header__wrapper}>
                <NavBar/>
                {/* <Tools/> */}
            </div>
            
        </header>
    );
}