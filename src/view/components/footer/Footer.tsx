import styles from "./sryles/footer.module.scss"

export default function Footer(){
    return(
        <footer className={styles.footer}>
            <div className={styles.footer__container}>
                <span className={styles.copywrite}>Developed by Artem Smirnov for Protei's IT-school</span>
                <span className={styles.copywrite}>summer 2024</span>
            </div>
        </footer>
    );

}