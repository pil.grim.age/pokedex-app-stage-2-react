import IPokemon from "@/model/transport/template/pokemon";
import styles from "./styles/pokemonProperties.module.scss"
import styles2 from "./styles/pokemon.module.scss"
import { typeColors } from "./styles/getNeonBorderStyles";

interface PokemonPropertiesProps{
    pokemon:IPokemon;
}

export default function PokemonProperties({pokemon}:PokemonPropertiesProps){
    const mainClass: string = `${styles2.pokemon} ${styles.properties} ${styles.cardProperties}`;
    // const mainClass: string = `${styles.properties}`;
    if(!pokemon){
        return(<></>);
    }

    return(
        <div className={mainClass}>
            <h4 className={styles.heading}>Name: <span>{pokemon.name}</span></h4>
            <h4 className={styles.heading}>ID: <span>{pokemon.id}</span></h4>
            <h4 className={styles.heading}>Types:</h4>
            <ul className={styles.list}>
                {pokemon.types.map((typeInfo, index) => {
                    return <li key={index} style={{color:`${typeColors[typeInfo.type.name]}`}}>{typeInfo.type.name}</li>
                })}
            </ul>
            <h4 className={styles.heading}>Abilities:</h4>
            <ul className={styles.list}>
                {pokemon.abilities.map((abilityInfo, index) => (
                    <li key={index}>{abilityInfo.ability.name}</li>
                ))}
            </ul>
        </div>
    )
}