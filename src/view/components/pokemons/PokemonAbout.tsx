import { useEffect, useState } from "react";
import styles from "./styles/pokemonDeck.module.scss"
import useFetching from "@/view/hooks/useFetching";
import Loader from "./../UI/Loader";
import IPokemon from "@/model/transport/template/pokemon";
import DeckSpace from "../UI/DeckSpace";
import Heading from "../UI/Heading";
import NoPokemons from "./NoPokemons";
import service from "@/service/pokemonService";
import Pokemon from "./Pokemon";
import store from "@/service/pokemonStoreService";
import PokemonProperties from "./PokemonProperties";
import stylesabout from "./styles/pokemonAbout.module.scss"

interface PokemonAboutProps{
    id:string;
}

export default function PokemonAbout({id}:PokemonAboutProps){
    const [pokemon, setPokemon] = useState(null);
    const [date, setDate] = useState(["Not Caught",""]);
    
    const [fetching, isLoading, error] = useFetching(fetchPokemon);

    useEffect(() => {
        fetching();
        if (store.check(parseInt(id))){
            catchPokemon();
        }
    }, []);

    async function fetchPokemon():Promise<void>{
        const newPokemon:IPokemon = await service.getPokemonById(id);
        setPokemon(newPokemon);
    }

    function catchPokemon(){
        setDate([`Caught`, store.getDate(parseInt(id))]);
    }

    return(
        <div className={`${styles.pokemonDeck} ${stylesabout.about}`}>
            
            <PokemonProperties pokemon={pokemon}/>  
            <Pokemon pokemon={pokemon} catchBack={catchPokemon} />

            <div className={stylesabout.paper}>
            <DeckSpace>
                {isLoading && <Loader/> || (!pokemon) && <NoPokemons/> || <Heading>{date[0]}<br/><span>{date[1]}</span></Heading>}
            </DeckSpace>
            </div>
        </div>
    );
}