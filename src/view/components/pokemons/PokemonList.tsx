import IPokemon from "@/model/transport/template/pokemon";
import Pokemon from "./Pokemon";
import styles from "./styles/pokemonList.module.scss"

interface PokemonListProps{
    pokemons:Array<IPokemon>;
    isLoading:boolean;
}

export default function PokemonList({pokemons, isLoading}:PokemonListProps){
    return(
        <ul className={styles.pokemonList}>
            {
                pokemons.map(pokemon => (
                    <li className={styles.pokemonList__item} key={pokemon.name}>
                        <Pokemon pokemon={pokemon}/>
                    </li>
                ))
            }
        </ul>
    );
}