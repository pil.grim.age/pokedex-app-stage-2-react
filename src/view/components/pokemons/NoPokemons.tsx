import Heading from "../UI/Heading";

export default function NoPokemons(){
    return(
        <Heading>No pokemons!</Heading>
    );
}