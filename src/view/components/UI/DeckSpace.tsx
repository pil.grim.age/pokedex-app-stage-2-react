import styles from "./styles/deckSpace.module.scss"

interface DeckSpaceProps{
    children: React.ReactNode;
}

export default function DeckSpace({children}:DeckSpaceProps){

    return(
        <div className={styles.container}>
            {children}
        </div>
    );

}