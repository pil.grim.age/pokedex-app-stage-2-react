import styles from "./styles/loader.module.scss"

export default function Loader(){

    return(
        <div className={styles.loader}>
            <div className={styles.loader__imgContainer}>
                <img className={styles.loader__img} src="https://usagif.com/wp-content/uploads/loading-50.gif" alt="..." />
            </div>
            <div className={styles.loader__text}>
                loading...
            </div>
        </div>
    );
}