import styles from "./styles/toolButt.module.scss"


interface ToolButtProps{
    name:string;
    icon:string;
    event:Function;
}

export default function ToolButt({name, icon, event, ...props}:ToolButtProps){
    return(
        <button className={styles.button} title={name} {...props}>
            <img className={styles.button__icon} src={icon} alt={name} />
        </button>
    );
}