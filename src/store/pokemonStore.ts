import IPokemonStore from "@/model/store/pokemonStore";

class PokemonStore implements IPokemonStore {
    private static instance: PokemonStore;
    private pokemons: Map<number, string>;

    private constructor() {
        this.pokemons = new Map();
        this.loadFromLocalStorage();
    }

    public static getInstance(): PokemonStore {
        if (!PokemonStore.instance) {
            PokemonStore.instance = new PokemonStore();
        }
        return PokemonStore.instance;
    }

    public catch(id: number, date: string): void {
        this.pokemons.set(id, date);
        this.saveToLocalStorage();
    }

    public contains(id: number): boolean {
        return this.pokemons.has(id);
    }

    public get(id: number): string {
        return this.pokemons.get(id);
    }

    public getAll(): number[] {
        return Array.from(this.pokemons.keys());
    }

    private saveToLocalStorage(): void {
        const pokemonsArray = Array.from(this.pokemons.entries());
        localStorage.setItem('pokemons', JSON.stringify(pokemonsArray));
    }

    private loadFromLocalStorage(): void {
        const pokemonsString = localStorage.getItem('pokemons');
        if (pokemonsString) {
            const pokemonsArray = JSON.parse(pokemonsString) as [number, string][];
            this.pokemons = new Map(pokemonsArray);
        }
    }
}

export default PokemonStore.getInstance();